import chai from 'chai';
import chaiJsonSchema = require('chai-json-schema');
import chaiHttp = require('chai-http');

chai.use(chaiJsonSchema);
chai.use(chaiHttp);

const expect = chai.expect;

let baseUrl = "http://www.boredapi.com";

let activitySchema = {
    title: 'activity schema',
    type: 'object',
    required: [
        "activity",
        "accessibility",
        "type",
        "participants",
        "price",
        "link",
        "key"
    ],
    properties: {
        activity: {
            type: "string"
        },
        accessibility: {
            type: ["number", "string"],
            default: ""
        },
        type: {
            type: "string",
            enum: ["relaxing", "busywork", "cooking", "social", "education", "charity", "diy", "relaxation", "recreational", "music"],
            default: ""
        },
        participants: {
            type: "number",
            default: 0
        },
        price: {
            type: ["number", "string"],
            default: ""
        },
        link: {
            type: "string",
            default: ""
        },
        key: {
            type: "string",
            default: ""
        }

    }
};

let errorSchema = {
    title: 'activity error schema',
    type: 'object',
    required: [
        "error",
    ],
    properties: {
        error: {
            type: "string"
        },
    }
};

function jsonBodyValidation(bodyObj: any, positive: boolean) {
    if (positive) {
        expect(bodyObj).to.jsonSchema(activitySchema);
        if (typeof bodyObj.accessibility === 'string')
            expect(bodyObj.accessibility).has.length(0);
        else
            expect(bodyObj.accessibility).to.be.within(0, 1);

        expect(bodyObj.participants).to.be.within(0, Number.POSITIVE_INFINITY);

        if (typeof bodyObj.price === 'string')
            expect(bodyObj.price).has.length(0);
        else
            expect(bodyObj.price).to.be.within(0, Number.MAX_VALUE);
    } else {
        expect(bodyObj).to.jsonSchema(errorSchema);
    }
}

describe('Activity validation', () => {
    it('Positive Activity Test', async () => {
        const response = await chai.request(baseUrl).get('/api/activity');

        expect(response).to.be.json;

        let bodyObj = response.body;

        jsonBodyValidation(bodyObj, true);

        console.log(bodyObj);
    });
});

describe('Activity key validation', () => {
    it('Positive Activity key test', async () => {
        const randomActivityResponse = await chai.request(baseUrl).get('/api/activity');
        expect(randomActivityResponse).to.be.json;
        let activityObj = randomActivityResponse.body;
        expect(activityObj).to.jsonSchema(activitySchema);

        const specifiedActivityResponse = await chai.request(baseUrl)
            .get('/api/activity')
            .query({key:activityObj.key});
        expect(specifiedActivityResponse).to.be.json;
        let bodyObj = specifiedActivityResponse.body;
        jsonBodyValidation(bodyObj, true);
        console.log(bodyObj);
    });

    it('Negative Activity key test', async () => {
        const response = await chai.request(baseUrl)
            .get('/api/activity')
            .query({key: 1});
        expect(response).to.be.json;
        let bodyObj = response.body;
        jsonBodyValidation(bodyObj, false);
        console.log(bodyObj);
    });
});

describe('Activity type validation', () => {
    it('Positive Activity type test', async () => {
        const randomActivityResponse = await chai.request(baseUrl).get('/api/activity');
        expect(randomActivityResponse).to.be.json;
        let activityObj = randomActivityResponse.body;
        expect(activityObj).to.jsonSchema(activitySchema);

        const specifiedActivityResponse = await chai.request(baseUrl)
            .get('/api/activity')
            .query({type:activityObj.type});
        expect(specifiedActivityResponse).to.be.json;
        let bodyObj = specifiedActivityResponse.body;
        jsonBodyValidation(bodyObj, true);
        console.log(bodyObj);
    });

    it('Negative Activity type test', async () => {
        const response = await chai.request(baseUrl)
            .get('/api/activity')
            .query({type: "1"});
        expect(response).to.be.json;
        let bodyObj = response.body;
        jsonBodyValidation(bodyObj, false);
        console.log(bodyObj);
    });
});
