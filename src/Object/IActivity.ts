interface IActivity {
    activity: string;
    accessibility: number | string;
    type: AcitvityType;
    participants: number;
    price:  number | string;
    link: string;
    key: string;
}

enum AcitvityType {
    "busywork",
    "social",
    "education",
    "charity",
    "diy",
    "relaxation",
    "recreational"
}